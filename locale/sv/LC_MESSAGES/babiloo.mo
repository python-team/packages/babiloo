��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   K     U     m     u     y     �     �     �     �     �     �     �     �     �  |   �     h     u  
   �     �     �     �     �     �     �     �     �     �     �                     2     8     L     i     w  
   �     �  &   �      �  	   �     �  	   �     �     	          "     6     >  >   L     �     �     �     �     �  	   �     �     �     �          /     N     W  	   i     s          �     �     �     �     �     �  
   �  
             (  !   7     Y     b     p     �     �  "   �     �     �     �     �       
             .     L     f     w  !   �     �  	   �  '   �  C   �     2     Q     l  �   r  r   !  K   �     �     �     �          0     <     @     `     m     q     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-09-24 20:08+0000
Last-Translator: Erik Simmesgård <Unknown>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekunder %s blev inte laddad: %s &Avbryt &Om &Om Babiloo &Stäng &Konfigurera Babiloo &Fil &Hjälp &Licensavtal &Avsluta &Spara till fil &Sök &Inställningar En ny version av Babiloo finns tillgänglig.
 Ny version: %s
 Nuvarande version: %s

Vill du ladda ned den nya versionen nu? &Författare Förkortning Om Babiloo Varning Utseende Utseende Artiklar Komplettera automatiskt Babilooinställningar Bakgrund Beteende Fet Bläddra... Bygger huvuddialog... Avbryt Skiftlägeskänsliga Rensa Rensa sökfältetet Klicka för att välja färg Standardhöjd Standardvidd Ordböcker Namn Ordbok som inte kunde installeras:
 %s Katalog som lagrar ordböckerna: Göm inte Ned Ladda ned Ladda ned ordböcker Ladda ned ordbok Hämtar Laddar ned filer... Element Felmeddelande Fel vid inläsning utav ordbok.
Fil korrupt eller inte stödd. Fel vid inläsning av ordet %s Exempel Förklaring Exportera ordbok som... Font Förgrund Från länk: Från lista: Globala inställningar Gå till &nästa artikel Gå till &föregående artikel Historik Ignorera accenter Importera Information &Installera från fil... Installerade ordböcker Programspråk: Kursiv Länkadress är tom Laddar %s... Hantera ordböcker... Flytta ned Flytta upp Ny version upptäckt Öppna katalog Vänligen ladda en ordbok först. Pluginer Popupfönster Förhandsgranska Skriv u&t artikel Uttala ordet Uttala ord med det här kommandot: Rapportera problem Uppdaterar ordböcker... Sök markering Sök Välj element Välj font Välj fontstorlek Kort användningsinformation: Visa om ord inte hittades Visa information Visa information om ordbok Visa endast om modifierare trycks Storlek Säg &ord Påbörjar nedladdningen (%s bytes) ... Det nya språket kommer användas efter programmet har startats om. Ordet <b>%s</b> hittades inte. Tid innan text försvinner Titel För att installera en ny ordbok från Internet, välj <b>Hantera ordböcker...</b> under <b>Ordböcker</b> menyn och välj sedan en från <b>Ladda ned ordböcker</b> fliken. För att installera en ny ordbok från en fil, välj <b>Installera från fil...</b> från <b>Ordböcker</b> menyn. För att börja använda en ordbok, välj en från <b>Ordböcker</b> menyn. Antal artiklar Transkribering Översätt Babiloo... Översätt detta program... Transparens Typ Ofömögen att ladda ned fil %s Understruken Upp Besök hemsidan 