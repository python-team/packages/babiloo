��    N      �  k   �      �     �     �     �     �     �     �     �     �       
     	              -  
   >     I     R     W     o     ~     �     �     �  
   �     �     �     �     �  E   �     ;     C     O     g  
   l     w     �     �     �     �     �     �     �     �  	   	     	     	     9	     A	     N	     V	     e	  #   x	     �	     �	     �	     �	     �	     �	     
  	   
  !   "
     D
     b
     g
  @   s
      �
  $   �
     �
  �      l   �  H   �     @     O     ]     {     �  	   �     �  �  �  	   4     >     B     G     V  
   c     n  
   z     �     �     �     �     �     �  	   �     �     �  '   �          4     D     S  
   `     k     o     �     �  N   �     �  
   �     �  
                  2     J     b     q     v     �     �     �     �     �     �     �     �     �     �       %        9     M     _     m     ~     �     �     �     �  "   �  	          7   %      ]  (   ~     �  }   �  `   +  D   �     �     �     �                    *                    A      F   9           &   "          %   =         $   -   ;       *       G       4           8           :       ,              7   C   0      /       '      K             3   >            (      B   M          1                  2      
   .   J   ?          )       I   !   L      #   +   <   E       	   6             5      N                   @             H       D         seconds &About &Close &License Agreement &Save to file A&uthors Abbreviation About Babiloo Alert Appearance Apperance AutoComplete Babiloo Settings Background Behavior Bold Building main dialog... Case sensitive Click to select color Default height Default width Dictionary name Don't hide Down Download Dictionaries Element Error Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground Global settings Go to &next article Go to &previous article Ignore accents Info Installed Dictionaries Interface Language: Italic Loading %s... Move down Move up Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Scan selection Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Transparency Type Underline Up Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-02-05 14:24+0000
Last-Translator: AJenbo <anders@jenbo.dk>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekunder &om &luk &licens aftale &gem til fil Forfattere forkortelse om Babiloo besked udseende udseende AutoUdførelse Babiloo indstillinger baggrund opførsel fed bygger hoved dialogen... Følsom overfor store og små bogstaver klik for at vælge farve standart højde standart brede Ordbogs navn gemte ikke ned download ordbøger element Fejl Fejl ved ordbogs overførelse
filen er enten korupteret eller ikke supporteret eksempel forklaring exporter orbog som... skrifttype forgrund Globale indstillinger gå til &næste artikel Gå til &forige artikel Ignorer accent info installer odbøger Interface sprog Kursiv loader %s... ryk ned ryk op load veligst en ordbog først plugins popup vindue smug kig Prin&t artikel udtal ordet Udtal ord ved hjælp af denne komando Raporter et problem scan udvælgelsen vælg element vælg skrifttype Vælg skriftstørelse kort bruger information vis hvis ordet ikke blev fundet vis info vis information om ordbog vis kun hvis modifikatoren trykkes størelse sig &ord Det nye sprog vil blive vist når programmet genstartes ordet <b>%s</b> blev ikke fundet timeout før gem efter musen føres over Titel For at installere en ny ordbog fre internettet, vælg <b>orden ordbøger...</b> og vælg en fra <b>download ordbøger</b>tab. for at installere en ny orbog fra en fil, vælg <b>installer fil... </b>fra<b>ordbogs</b>menuen. For at begynde brugen af ordbogen, vælg en af <b>ordbøger</b> menu Alle artikler transkription oversæt denne applikation gennemsigtighed tast understreget op 