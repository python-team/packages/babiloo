��    h      \  �   �      �     �     �     �     �     �     �     		     	     	     (	     .	     <	  	   B	     L	     U	     b	     p	  
   v	  	   �	     �	     �	  
   �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     &
  *   6
  #   a
  
   �
     �
     �
     �
     �
     �
     �
     �
  E   �
     7     ?     K     c  
   h  
   s  
   ~     �     �     �     �     �     �     �     �     �          (     /     E     S  	   j     t     |     �     �     �     �  #   �     �          #     2     9     H     T     e     ~  	   �  !   �     �     �     �  $   �  @         V  $   w     �  �   �  H   ,     u     �     �     �     �     �     �  	   �            �       �     �     �  %   �       #   2     V     f  (   y     �     �     �     �       %     %   4     Z     p     �  	   �      �     �     �     �  "     <   &     c  U   }  (   �  +   �     (  &   >  a   e  U   �          4     A  +   W  +   �     �  2   �     �  �        �     �  T   �     G     Z     s     �  +   �  *   �  -   �       :   -     h     u     �     �  #   �     �  *     '   ,  8   T      �     �  G   �       !   4     V  "   s  [   �  (   �  8        T  	   n     x  "   �  6   �  0   �  C      (   d  \   �  X   �     C     P  :   j  �   �  ?   9  f   y     �    �  �   !     �!     �!  '   "  5   ,"     b"     �"  R   �"     �"  	   �"     #     N               Z               -   G          +   c      S      K      )   3   B   *           T   4   
      M   2       	   /   A      :       !   '   9       "   @   (   _          J      R          \         &          6   X      <              P   ^          7      a       E   I   H   ?   ;   F                                   Y      0   `   ,       U              %   e       g   d   b      5   O                W   =              h       #       $       ]   [       .   D   1       L   C      V   >   f   Q   8     seconds &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles Babiloo Settings Background Behavior Bold Browse... Building main dialog... Clear Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up Please load a dictionary first. Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-05-05 15:17+0000
Last-Translator: Varun <varun.wipro@gmail.com>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  सेकंड &अंत करना बारे में Babiloo के बारे में बंद करना कॉन्फ़िगर Babiloo फ़ाइल सहायता लाइसेंस अनुबंध बंद करना फ़ाइल में Save स्कैन सेटिंग्स लेखक संक्षिप्त नाम Babiloo के बारे में चेतावनी प्रकटन प्रकटन लेख Babiloo सेटिंग्स पृष्ठभूमि व्यवहार बोल्ड ब्राउज़ करें बिल्डिंग मुख्य संवाद ... साफ़ करना रंग का चयन करने के लिए क्लिक करें डिफ़ॉल्ट ऊंचाई डिफ़ॉल्ट चौड़ाई शब्दकोश शब्दकोश का नाम शब्दकोश कि स्थापित नहीं किया जा सका:
%s निर्देशिका भंडारण के शब्दकोशों: मत छिपाओ नीचे डाउनलोड डाउनलोड शब्दकोश शब्दकोश डाउनलोड डाउनलोड फ़ाइलों को डाउनलोड तत्व त्रुटि के शब्दकोश लोड करने की कोशिश कर रहा.
या भ्रष्ट संचिका समर्थित नहीं. उदाहरण व्याख्या निर्यात की डिक्शनरी के रूप में ... फ़ॉन्ट अग्रभूमि Link से: सूची से: ग्लोबल सेटिंग्स अगले लेख पर जाएँ पिछले लेख पर जाएँ इतिहास उच्चारण पर ध्यान न दें आयात जानकारी Install फ़ाइल से Installed शब्दकोश अंतरपटल भाषा: इटैलिक लिंक पता खाली है %s लोड हो रहा है .. प्रबंधित शब्दकोशों ... नीचे ले जाएँ ऊपर ले जाएँ कृपया एक डिक्शनरी पहले लोड. पॉपअप विंडो पूर्वावलोकन प्रिंट लेख उच्चारण शब्द उच्चारण शब्द इस आदेश का उपयोग करके: रिपोर्ट समस्या स्कैनिंग शब्दकोशों ... स्कैन चयन खोज चुनें तत्व फ़ॉन्ट चुनें चुनें फ़ॉन्ट का आकार लघु उपयोग जानकारी: दिखाएँ यदि शब्द नहीं मिला जानकारी दिखाएँ शब्दकोश के बारे में जानकारी दिखाएँ दिखाएँ सिर्फ अगर आपरिवर्तक दबाया आकार बोलो शब्द डाउनलोड शुरू (%s बाइट्स) इस नई भाषा के कार्यक्रम restarting के बाद प्रदर्शित किया जाएगा. यह वचन <b>% s </ b> नहीं मिला है. Timeout से पहले माउस खत्म होने के बाद छिपाना शीर्षक प्रबंधित शब्दकोश इंटरनेट, चयन से एक नया शब्दकोश स्थापित करने के लिए ... और एक के  डाउनलोड शब्दकोश टैब से चुनें. कॉपी पाठ इस  शब्दकोशों में मेनू से डिक्शनरी, चयन एक का उपयोग आरंभ करने के लिए. कुल लेख प्रतिलेखन अनुवाद यह Application.. अनुवाद इस अनुप्रयोग पारदर्शिता टाइप फ़ाइल %s डाउनलोड करने में असमर्थ रेखांकित ऊपर मुखपृष्ठ 