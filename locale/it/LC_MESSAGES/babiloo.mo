��    D      <  a   \      �     �     �     �     �     �     �            	        "     /  	   3     =  
   ?     J     S     X     ^     s  	   �     �     �     �     �     �     �     �  
   �     �     �                      
   $     /     ?     A     F  	   M     W     _     g     t     |  #   �     �     �     �     �     �     �     �  	     !        >     \     z           �  $   �     �     �     �  	   �     �     �  �  �     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	                  3  	   S     ]     r     z     �     �  	   �     �     �     �     �     �     �     �  	   �               )     +     8     @     O  
   \     g  	   y     �  *   �     �     �     �     �     �       %   $     J  "   ^  #   �     �  
   �     �  )   �  +        /     6     C     E     R     U     @       2          '      5   )          #   	             C       !   3       ,      <          ;           A   =          4      6      $           *               8          B      &          :             0      >   "   %   ?                 /           7                       
                  .          -   (         1   D         9   +            #000000 % &About &File &Help &Quit &Save to file &Scan &Settings Abbreviation Alt Apperance B Background Behavior Bold Clear Clear the search box Click to select color Configure Configure plugin Control Ctrl+Q Default height Default width Dictionaries Dictionary name Don't hide Down Element Error Example Explanation Font Foreground Global settings I Info Italic Move down Move up Plugins Popup window Preview Pronounce the word Pronounce words using this command: Scan selection Search Select element Select font Select font size Shift Show if word not found Show info Show information about dictionary Show information about plugin Show only if modifier pressed Size Speak &word The word <b>%s</b> is not found. Timeout before hide after mouse over Title Transcription U Underline Up Win Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-01-22 14:30+0100
PO-Revision-Date: 2009-01-09 18:00+0000
Last-Translator: Ivan Garcia <capiscuas@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-02-08 12:44+0000
X-Generator: Launchpad (build Unknown)
 #000000 % I&nformazioni su &File &Aiuto &Esci &Salva come &Cerca &Impostazioni Abbreviazione Alt Aspetto B Sfondo Comportamento Grassetto Cancella Cancella la ricerca Click per selezionare il colore Configura Configura estensione Control Ctrl+Q Altezza di default Ampiezza di default Dizionari Nome del dizionario Non nascondere Giù Elemento Errore Esempio Spiegazione Carattere Primo piano Impostazioni globali I Informazioni Corsivo Sposta in giù Sposta in su Estensioni Finestra di popup Anteprima Pronuncia la parola Pronuncia le parole usando questo comando: Cerca selezione Cerca Seleziona elementi Seleziona carattere Seleziona dimensione carattere Shift Mostra se la parola non viene trovata Mostra informazioni Mostra informazioni sul dizionario Mostra informazioni sull'estensione Mostra solo se si preme Dimensione Pronuncia f&rase La parola <b>%s</b> non è stata trovata. Tempo di scomparsa dopo aver mosso il mouse Titolo Trascrizione U Sottolineato Su Win 