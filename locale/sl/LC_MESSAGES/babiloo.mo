��    _                      	               (     /     B     H     N     a     g     u  	   {     �     �     �     �  
   �  	   �     �     �     �  
   �     �     �     	     	     +	     1	     F	     \	     k	     y	     �	  
   �	     �	     �	     �	     �	  E   �	     
     
     $
     <
  
   A
     L
     \
     p
     �
     �
     �
     �
     �
     �
     �
     �
     �
        	        !     )     I     Q     ^     f     u  #   �     �     �     �     �     �     �     �       	   /  !   9     [     y     ~  @   �      �  $   �       �     l   �  H        W     f     t     �     �  	   �     �     �  �  �     d     l  
   o     z     �  	   �     �     �     �     �  	   �     �     �  
   �  	      	   
               "     *     ?     R     Y     a     h  (   �     �     �     �     �     �                    #     '     6     >  G   E     �     �     �     �     �     �     �     �  	             -     3     ?     V     k     }     �     �     �     �     �     �     �  	               %   -     S     c     r     y     �     �     �     �     �     �  )        I     R  )   c  !   �  &   �     �  �   �  j   d  <   �                 
   5     @  
   G     R     V     A   Y   *      0   Q         /   I           3      =          X       ,   M   B                  %          5              )               +           T          :   ^           S   8                     \       6   2               _          K   ?   9                '   U   
   F       [   "       -   N         #         &   W   >   P   E   .   $   V      G      !   H   R              ]   1   C   4   @       L          ;   <   Z       J   (   O       D       	   7        seconds &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Building main dialog... Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Don't hide Down Download Dictionaries Element Error Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Loading %s... Manage Dictionaries... Move down Move up Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Transparency Type Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-03-25 17:34+0000
Last-Translator: SkyKnight <jani.zorz@gmail.com>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekund %O &O Babiloo &Zapri &Konfiguriraj Babiloo &Datoteka &Pomoč &Licenčna pogodba &Izhod &Shrani v datoteko &Skeniraj &Nastavitve A&vtorji Okrajšava O Babiloo Opozorilo Izgled Izgled Članki Dokončaj avtomatsko Babiloo nastavitve Ozadje Vedenje Krepko Gradim glavni dialog... Občutljivo na velike in male začetnice Počisti Počisti polje za iskanje S klikom izberi barvo Privzeta višina Privzeta širina Slovarji Ime slovarja Ne skrij Dol Snemi slovarje Element Napaka Napaka pri nalaganju slovarja.
Datoteka poškodovana ali pa ni podprta. Primer Razlaga Izvozi slovar kot... Pisava Ospredje Splošne nastavitve Pojdi na &naslednji članek Pojdi na &prejšnji članek Zgodovina Prezri naglasna mesta Uvozi Informacije Namesti iz datoteke... Nameščeni Slovarji Vmesniški jezik: Ležeče Nalagam %s... Upravljaj s Slovarji... Premakni dol Premakni gor Prosim najprej naloži slovar. Priključki Nezahtevano okno Predogled Natisni &članek Izgovori besedo Izgovori besede z uporabo tega ukaza: Prijavi Problem Skeniraj izbor Išči Izberi element Izberi pisavo Izberi velikost pisave Informacije za trenutno rabo: Prikaži če beseda ni najdena Prikaži informacijo Prikaži informacijo o slovarju Prikaži le če je modifikator pritisnjen Velikost Izgovori &besedo Nov jezik bo prikazan po ponovnem zagonu. Beseda <b>%s</b> ni bila najdena. Čas pred skritjem po preletu z miško Naslov Za namestitev novega slovarja z interneta, izberi <b>Upravljaj s slovarji...</b> in si poišči enega iz <b>Snemi Slovarje</b> tabele. Za namestitev novega slovarja iz datoteke, izberi <b>Namesti iz datoteke...</b> iz <b>Slovarji</b> menuja. Za uporabo slovarja, izberi enega iz <b>Slovarji</b> menija. Vsi članki Prepis Prevedi to aplikacijo Prozornost Tipkaj Podčrtano Gor Obišči Domačo stran 