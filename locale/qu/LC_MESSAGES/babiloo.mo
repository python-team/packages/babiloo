��    s      �  �   L      �	     �	     �	     �	     �	     �	     �	     
     
     
     #
     6
     <
     J
  	   P
     Z
     c
     p
     ~
  
   �
  	   �
     �
     �
     �
  
   �
     �
     �
  	   �
     �
     �
                    ,     B     Q     _     l  *   |  #   �  
   �     �     �     �     �               /     7  E   =     �     �     �     �     �  
   �  
   �  
   �     �               1     9     H     O     T     i     �     �     �     �     �  	   �     �     �     �          ,     4     A     I     X  #   k     �     �     �     �     �     �     �     �       	   -  !   7     Y     w     |  $   �  @   �      �  $        4  �   :  l   �  H   1     z     �     �     �     �     �     �  	         
       �       �     �  	   �  	   �     �               %     ,     5     B     K     ^     p     ~     �     �  	   �     �     �     �     �                '  
   =     H     Q     i      z     �     �     �     �     �     �       +        F     f     t  	   x     �     �     �     �     �     �  J   �  '   8     `     l      y  	   �     �     �  
   �     �     �     �          '     ?     S     c     y     �  	   �     �     �     �     �  	   �          !  ,   5     b     t     �     �     �      �     �  )   �          3     <     N     a     z  %   �     �     �  .   �            "   .  j   Q  #   �  .   �       9  !  �   [  �   F     �     �     �          -  
   C  %   N     t     �     �     f   &          E   J         e              ]          /   T   S   C       G   >       H       B      O   7       F                      I   l   g              n            @       Y   	       X   L   (          +   Q   r   j           4            ;   =   m           i   K   )   -       q           "   
      6   1      b   [      3                     '   h   5              s   ,      R   o           `       !   U       %       9       c   M   $       <          8      .              :   a   W   N   Z   ^   V      k   *   2           D   P   _         d       0   p   \   A   ?   #         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-06-07 13:36+0000
Last-Translator: amosbatto <amosbatto@yahoo.com>
Language-Team: Quechua <qu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  Sigundukuna %s no fue cargado: %s &Saqichiy &Kaymanta &Babiloomanta... &Wisqay &Babiloota kamachiy... &Khipu &Yanapay &Kamachikuna &Lloqsiy &Khiputa jallch'ay &Waqmanta rikusqa &Aqllasqakuna &Ruraqekuna Chikanyaynin Babiloomanta Paqtataq! &Qhawarichikusqanmanta &Qhawarichikusqanmanta &Willaykuna &Pachanmanta junt'achiy Babilooqpa aqllasqakunan Qhipa llinphi Imaynata kamachikunqa Yanachasqa &Mask'ay Wayranata ruwakushan... &Mana munaspaña Sullka &kuraq siq'ikunata qhaway Pichay Mask'asqata pichay Llinphita aqllanapaq q'apiy Pachanmanta sayasqa Pachanmanta kinrasqa &Simitaqikuna Simitaqiqpa sutin Mana churakuyta atinchu kay simitaqita:
 %s Maypi simitaqikuna jallch'asqa: Ama  pakaychu Ura Urayachiy Simitaqikunata &urayachiy... Simitaqita urayachiy Urayamushan Khipukunata urayamushan... Imaymana Pantasqa Simitaqi apachimuqtin pantarun.
Khipu waqllisqa utaq manan ñawichakunchu. Error mientras cargando la palabra "%s" Qhawarichiy Mast'ariynin Simitaqita apachiy kay jinata... Ima siq'i Ñawpaq llinphi Waskhamanta: Qutumanta: &Lliwmanta aqllasqakuna &Jamuq willayman riy Ñ&awpaq willayman riy &Ñawpaq mask'asqakuna K&allpachaqkunata saqiy Jawamanta apachimuy Imamanta willay &Khipumanta churay... Simitaqikuna &churasqa Programapaq simi: T'iksusqa Dirección de enlace es vacia Apamushan %s… Si&mitaqikunata kamachiy... Urachiy Wichachiy Nueva versión detectada Directoriota kichay Ama jina kaychu juk simitaqitaraq apachimuy. Lla&nk'apakuqkuna &Jatariq wayrana Ñawpaq qhawariy &Willayta ñit'iy Simita rimariy Kay qilqawan simikunata rimariy: &Pantayta willay Watiqmanta simitaqikunata ñawichashan... Watiqmanta aqllaqtin qawarichiy &Mask'ay Imaymanata aqllay Ima siq'ita aqllay Aqllay siq'iq sayayninta Pisi pachallata qhawanapaq Simi mana tarichichusqata qhawarichiy Ima kayninta qhawarichiy Simitaqemanta willay Kay q'apina aqllasqallas kashaqtin qhawarichiy Sayay Simita &rimariy (%s bytes) uramuyta qallarishan... Musuq simiqa qhawarichikunqa watiqmanta qallariqtin.
La nueva lengua será mostrada después de reiniciar. <b>%s</b> simiqa mana tarikusqachu. Jaykata suyanqa juk'uchaq t'uqsiynin pakanapaq Pata sutinchaynin Internetmanta simitaqita churanapaq, aqllay <b>Simitaqikunata kamachiy...</b> nisqata, jinallataq <b>Simitaqikunata urayachiy</b> nisqatawan. 
Para instalar un diccionario nuevo desde el internet, seleccione <b>Simitaqikunata kamachiy...</b> y escoja un diccionario de la pestaña <b>Simitaqikunata urayachiy</b>. Khipumanta simitaqita churanapaq aqllay <b>Simitaqikuna</b>, jinallataq <b>Qhipumanta churay...</b> nisqata. 
Para instalar un diccionario nuevo desde un archivo, seleccione <b>Qhipumanta churay...</b> en el menú <b>Simitaqikuna</b>. Simitaqipi qallarinapaq, <b>Simitaqikuna</b>manta jukta aqllay. 
Para empezar con un diccionario, seleccione uno del menú de <b>Simitaqikuna</b>. Llipin willaykuna Qilqa kikinchasqa Kay programata &t'ikray Kay programata &t'ikray Qhispichiskusqanmanta Ima kaynin Mana "%s" khiputa urayachiyta atinchu Uranpi siq'isqa Pata &Wasiy raphita watukuy 