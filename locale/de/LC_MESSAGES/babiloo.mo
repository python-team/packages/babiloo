��    D      <  a   \      �     �     �     �     �     �     �            	        "     /  	   3     =  
   ?     J     S     X     ^     s  	   �     �     �     �     �     �     �     �  
   �     �     �                      
   $     /     ?     A     F  	   M     W     _     g     t     |  #   �     �     �     �     �     �     �     �  	     !        >     \     z           �  $   �     �     �     �  	   �     �     �  �  �     �
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     
       	        "     '     0     D     \     j          �     �     �     �     �     �     �     �     �     �     �  
                   -     /     4  
   ;  	   F     P     X     f     o  ,   �     �     �     �     �     �     �  "        )  &   @  !   g      �     �     �  (   �  .   �       	   "     ,     .     <     @     @       2          '      5   )          #   	             C       !   3       ,      <          ;           A   =          4      6      $           *               8          B      &          :             0      >   "   %   ?                 /           7                       
                  .          -   (         1   D         9   +            #000000 % &About &File &Help &Quit &Save to file &Scan &Settings Abbreviation Alt Apperance B Background Behavior Bold Clear Clear the search box Click to select color Configure Configure plugin Control Ctrl+Q Default height Default width Dictionaries Dictionary name Don't hide Down Element Error Example Explanation Font Foreground Global settings I Info Italic Move down Move up Plugins Popup window Preview Pronounce the word Pronounce words using this command: Scan selection Search Select element Select font Select font size Shift Show if word not found Show info Show information about dictionary Show information about plugin Show only if modifier pressed Size Speak &word The word <b>%s</b> is not found. Timeout before hide after mouse over Title Transcription U Underline Up Win Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-01-22 14:30+0100
PO-Revision-Date: 2009-01-09 18:00+0000
Last-Translator: Ivan Garcia <capiscuas@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-02-08 12:44+0000
X-Generator: Launchpad (build Unknown)
 #000000 % Über Q&StarDict &Datei &Hilfe &Beenden In &Datei speichern &Scannen &Einstellungen Abkürzung Alt Erscheinungsbild B Hintergrund Verhalten Fett Löschen Das Suchfeld leeren Klicken zur Farbauswahl Konfigurieren Plugin konfigurieren Strg Strg+Q Standardhöhe Standardbreite Wörterbücher Wörterbuchname Nicht verstecken Ab Element Fehler Beispiel Erläuterung Schriftart Vordergrund Globale Einstellungen I Info Kursiv Nach unten Nach oben Plugins Popup-Fenster Vorschau Spreche das Wort Folgendes Kommando zur Aussprache verwenden: Auswahl scannen Suche Element wählen Schriftart wählen Schriftgröße wählen Umschalt Anzeigen falls Wort nicht gefunden Informationen anzeigen Informationen zum Wörterbuch anzeigen Informationen zum Plugin anzeigen Nur zeigen falls Taste gedrückt Größe &Wort aussprechen Das Wort <b>%s</b> wurde nicht gefunden. Zeitdauer bis zum Verschwinden nach Mouse Over Titel Abschrift U Unterstrichen Auf Win 