��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �     Q     ]     r     ~     �     �     �  	   �  	   �     �     �     �     
       �   $     �  
   �     �  
   �                    #     ?     T     [  
   j     u  2   �  
   �     �  
   �      �                ?     ^     m  0     6   �     �     �  
              %     ;     U     n  
   w  p   �  *   �  
        )  !   2     T     ]     f     v     �     �     �     �  7   �  
   %     0     9     R     p     �      �     �     �     �     �     
     )  -   =     k     x     �     �     �  4   �       &        B  
   Z     e     y     �  !   �  (   �     �  )   	  -   3     a     j  +     @   �  )   �  B     
   Y  �   d  �     l   �           /      6      R      p      }   +   �      �      �      �      g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-10-29 21:24+0000
Last-Translator: Yinon Ehrlich <yinoneh@gmail.com>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  שניות %s לא נטען: %s &ביטול &אודות &אודות Babiloo &סגירה ה&גדרת Babiloo &קובץ ע&זרה &הסכם רישוי י&ציאה &שמירה לקובץ &סריקה ה&גדרות אותרה גירסה חדשה של Babiloo.

גירסה חדשה: %s
גירסה נוכחית: %s

האם ברצונך להוריד את הגירסה החדשה כעת? יו&צרים קיצור אודות Babiloo התרעה חזות חזות מאמרים השלמה אוטומטית הגדרות Babiloo רקע התנהגות מודגש עיון... בניית תיבת הדו־שיח הראשית... ביטול תלוי רישיות ריקון ריקון תיבת החיפוש לחיצה לבחירת צבע גובה ברירת המחדל רוחב ברירת המחדל מילונים שם המילון מילון שאינו ניתן להתקנה:
 %s התיקייה בה מאוחסנים המילונים: ללא הסתרה מטה הורדה הורדת מילונים הורדת מילון בתהליכי הורדה מוריד קבצים... רכיב שגיאה ארעה שגיאה בעת הנסיון לטעינת המילון.
הקובץ פגום או שאינו נתמך. שגיאה בעת טעינת המילה %s דוגמה הסבר ייצוא המילון בשם... גופן חזית מהקישור: מהרשימה: הגדרות כלליות מעבר למאמר ה&בא מעבר למאמר ה&קודם היסטוריה התעלמות מסימנים מערב אירופאים ייבוא מידע התקנה מקובץ... מילונים מותקנים שפת המנשק: נטוי כתובת הקישור ריקה %s בתהליכי טעינה... ניהול המילונים... הזז מטה הזז מעלה אותרה גירסה חדשה מדריך פתוח נא לטעון את המילון תחילה. תוספים חלון מוקפץ תצוגה מקדימה ה&דפסת מאמר הגיית המילה הגיית המילים באמצעות הפקודה: דווח על תקלה המילונים נסרקים מחדש סריקת הבחירה חיפוש בחירת רכיב בחירת גופן בחירת גודל הגופן נתוני שימוש קצרים: יוצג אם המילה לא נמצאה הצגת מידע הצגת מידע אודות המילון הצגה רק אם נלחץ מקש החלפה גודל אמירת &מילה ההורדה מתחילה (%s בתים) ... השפה החדשה תוצג לאחר איתחול התוכנה. המילה <b>%s</b> אינה נמצאת. משך הזמן בטרם ההסתרה לאחר מעבר העכבר כותרת כדי להתקין מילון חדש מהאינטרנט, יש לבחור ב־<b>ניהול מילונים...</b> ולבחור באחד מהלשונית <b>הורדת מילונים</b> כדי להתקין מילון חדש מקובץ, יש לבחור ב־<b>התקנה מקובץ...</b> מהתפריט <b>מילונים</b> כדי להתחיל להשתמש במילון, יש לבחור באחד מהתפריט <b>מילונים</b> סך כל המאמרים כתב תרגם יישום זה... תרגום יישום זה... שקיפות סוג לא ניתן לטעון את הקובץ %s קו תחתי מעלה בקר באתר הבית 