��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   c     m     �     �     �  	   �     �     �     �     �               .     6  �   J               (  
   <     G     T     a     p     �     �     �     �     �  ,   �       "        4     ;     X  #   u     �     �     �  4   �  6        J     X  
   a     l     �     �  %   �     �     �  e   �  (   Z     �  
   �  !   �     �     �     �     �       *      *   K     v     �     �     �     �  !   �                      A     R     m     �  1   �     �  -   �               <     I     ^  @   r     �  .   �     �               .     @  3   Y  1   �     �  +   �  <     
   B     M  '   ^  R   �  +   �  6        <  �   K  �   �  c   }      �      �   !   !  "   .!     Q!     b!  /   o!  
   �!     �!  "   �!     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-10-19 09:09+0000
Last-Translator: عاصم شلي (Assem Chelli) <assem.ch@gmail.com>
Language-Team: Arabic <ar@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  ثوان %s لم يتم تحميله: %s إ&جهاض &حول &حول بابيلو أ&غلق اع&داد بابيلو &ملف &مساعدة ا&تفاقية الترخيص إ&نهاء &حفظ في ملف &فحص الإ&عدادات تم اصدار نسخة جديدة من بابيلو.

الاصدار الجديد : %s
الاثدار القديم : %s

هل تريد تنزيل الاصدار الجديد الآن؟ ال&مؤلفين الاختصار حول بابيلو تنبيه المظهر المظهر الكلمات اكمال تلقائي إعدادات بابيلو الخلفيّة السلوك عريض استعرض... بناء النافذة الرئيسية ... ألغِ مراعاة حالة الأحرف مسح امسح مربع البحث انقر لتحديد لون الارتفاع الافتراضي العرض الافتراضي القواميس اسم القاموس القاموس الذي تعذّر تثبيته:
 %s المجلّد يقوم بتخزين القواميس: لا تخفي أسفل تنزيل تنزيل القواميس تنزيل القاموس جاري التنزيل جاري تنزيل الملفات... العنصر خطأ خطأ اثناء محاولة تحميل القاموس.
الملف تالف أو غير معتمد. خطأ عند تحميل الكلمة %s مثال الشرح تصدير القاموس كـ... الخط الواجهة الأمامية من الرابط: من قائمة: إعدادات شاملة اذهب الى المقال ال&لاحق اذهب الى المقال ال&سابق المحفوظات تجاهل اللهجات استيراد المعلومات تثبيت من ملف... القواميس المثبّتة لغة الواجهة مائل عنوان الرابط فارغ تحميل %s... أدر القواميس... حرّك لأسفل حرّك لأعلى تمّ اكتشاف وجود نسخة  جديدة افتح مجلداً الرجاء تحميل قاموس أوّلا الملحقات النافذة المنبثقة معاينة &طباعة مقال نطق الكلمة قم بنطق الكلمات باستعمال هذا الأمر: أبلغ عن مشكلة جاري اعادة فحص القواميس... فحص التحديد بحث إختيار عنصر إختيار خط حدّد حجم الخط معلومات الاستعمال الاساسية: اظهر في حالة عدم ايجاد كلمة عرض المعلومات عرض المعلومات حول قاموس اظهر فقط اذا تم الضغط على المغيّر الحجم نطق &كلمة بدء التنزيل (%s بايت) ... اللغة الجديدة ستعرض بعد اعادة تشغيل البرنامج الكلمة <b>%s</b> غير موجودة. مهلة الاخفاء بعد مرور بالفأرة العنوان لتثبيت قاموس جديد من الانترنت، <b>اختر أدر القواميس...</b> و اختر واحدا من لسان <b>تنزيل القواميس</b>. لتثبيت قاموس جديد من ملف ، اختر <b>ثبّت من ملفّ</b> ... من قائمة  <b>القواميس</b>. للبدء باستعمال قاموس،اختر واحدا من <b>قائمة القواميس</b> مجموع المقالات الكتابة ترجم هذا التطبيق... ترجم هذا البرنامج.. الشفافية النّوع غير قادر على تنزيل الملف %s تسطير أعلى زر الصفحة الرئيسية 