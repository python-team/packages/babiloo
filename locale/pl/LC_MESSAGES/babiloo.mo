��    n      �  �   �      P	     Q	     Z	     b	     d	     k	     z	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
  
   	
  	   
     
     '
     4
     6
     >
  
   O
     Z
     c
     h
     w
     }
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
  
                  2     :  E   @     �     �     �     �  
   �     �     �     �     �                         >     C     X     o     �     �     �  	   �     �     �     �     �     �     �       #         D     U     d     k     z     �     �     �     �  	   �  !   �     �          5     :  @   F      �  $   �     �  �   �  l   ]  H   �          "     0     N     [     `  	   b     l     o     ~  �  �                      
   &     1     :     O     U  	   \     f     o     �     �     �     �     �  	   �     �     �     �     �  	   �     �     �     �     �       
   
  
     !      	   B     L     k  
   �     �     �     �     �     �  	   �     �     �          	          #  H   *  	   s     }     �     �     �     �  "   �  #   �                    /  '   6     ^     c     y     �     �     �     �     �     �  %   �       
   '     2     ;     N  (   \     �     �     �     �     �     �     �     �  ,        9     E     c  5        �     �  :   �  )     /   1     a  �   h  c   �  J   M     �     �     �     �     �     �     �     �               h       :   M   
                `          *   8   %      G   J   <           j   A   e   f   d           D   C       '       )   3   @              K   i             N   (                                #       V   c                  &       4      _   ?   $   6   2                    O   W          -               [   X   9      5       k   Q      ]      m              P       S       ^   "       U   F   I   +          E   .   >      7          a   /       Z   !       \   	   n   H      B       1   T      =   L      R   l   ,   Y   0       g   b   ;        seconds #000000 % &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings ... A&uthors Abbreviation About Babiloo Alert Alt Appearance Apperance Articles AutoComplete B Babiloo Babiloo Settings Background Behavior Bold Case sensitive Clear Clear the search box Click to select color Configure Configure plugin Control Ctrl+Q Default height Default width Dictionaries Dictionary name Don't hide Down Download Dictionaries Element Error Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground Global settings Go to &next article Go to &previous article History I Ignore accents Import Import one or many dictionaries Info Install from file... Installed Dictionaries Interface Language: Italic Loading %s... Manage Dictionaries... Move down Move up Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Scan selection Search Select element Select font Select font size Shift Short Usage Information: Show if word not found Show info Show information about dictionary Show information about plugin Show only if modifier pressed Size Speak &word The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Transparency Type U Underline Up Visit HomePage Win Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-01-22 14:30+0100
PO-Revision-Date: 2009-02-07 16:51+0000
Last-Translator: jeremiPL <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-02-08 12:44+0000
X-Generator: Launchpad (build Unknown)
  sekund #000000 % &O QStarDict &O Babiloo &Zamknij &Skonfiguruj Babiloo &Plik &Pomoc &Licencja &Zamknij &Zapisz do pliku &Skanuj &Ustawienia ... A&utorzy Skrót O Babiloo Alarm Alt Wygląd Wygląd Artykuły Autouzupełnianie B Babiloo Ustawienia Babiloo Tło Zachowanie Pogrubiona Rozróżniaj małe i duże litery Wyczyść Wyczyść okienko wyszukiwania Kliknij by wybrać kolor Konfiguruj Konfiguruj wtyczkę Control Ctrl+Q Domyślna wysokość Domyślna szerokość Słowniki Nazwa słownika Nie ukrywaj Dół Pobierz słowniki Element Błąd Błąd podczas ładowania słownika.
Plik zepsuty, lub nie obsługiwany. Przykład Wytłumaczenie Eksportuj słownik jako... Czcionka Pierwszy plan Globalne opcje Przejdź do &następnego artykułu Przejdź do &poprzedniego artykułu Historia I Ignoruj akcenty Import Zaimportuj jeden, lub wiele słowników Info Zainstaluj z pliku... Zainstalowane słowniki Język interfejsu: Pochyła Wczytywanie %s... Zarządzaj Słownikami Przesuń w dół Przesuń w górę Proszę najpierw załadować katalog. Wtyczki Okno popup Podgląd Wydruku&j artykuł Wymów słowo Wymów słowo używając tego polecenia: Zgłoś problem Skanuj zaznaczenie Szukaj Wybierz element Wybierz czcionkę Wybierz rozmiar czionki Shift Krótka instrukcja obsługi: Pokaż jeśli słowo nie zostało znalezione Pokaż info Pokaż informacje o słowniku Pokaż informacje o wtyczce Pokaż tylko jeśli modyfikator zostanie naciśnięty Rozmiar Wymów sło&wo Nowy język pojawi się po ponownym uruchomieniu programu. Słowo <b>%s</b> nie zostało znalezione. Opóźnienie zanim ukryje po najechaniu myszką Tytuł Żeby zainstalować nowy słownik z internetu, zaznacz "Zarządzaj słownikami...</b> i wybierz jeden z <b>Pobierz słowniki</b> By zainstalować nowy słownik z pliku, zaznacz <b>Instaluj z pliku...</b> z menu <b>Słowniki</b>. Żeby zacząć używać słownik, wybierz jakiś, z menu <b>Słowniki</b>. Wszystkie artykuły Transkrypcja Przetłumacz ten program... Przeźroczystość Typ U Podkreślona Góra Odwiedź stronę domową Win 