��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   I     S     p  
   |     �     �     �     �     �     �     �     �     �       �        �     �     �     �     �  	   �     �               6     D     Q  
   V  )   a     �     �     �     �      �     �     �            !   3  *   U     �     �     �     �     �     �      �  	        )  P   0  &   �     �     �      �     �  
   �     �          )     =     Y  
   y     �     �     �  #   �     �     �               '     ;     W     m     �     �  /   �     �     �     �            (   +     T     j     �  	   �     �     �     �  &   �  "        ;  -   U  *   �     �     �  (   �  E   �  !   6  /   X     �  �   �  �   p  g        u     �     �     �     �     �  )   �  	                     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-05-19 17:57+0000
Last-Translator: Mathieu Pasquet <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  secondes %s n'a pu être chargé : %s &Abandonner À &propos À &propos de Babiloo &Fermer &Configurer Babiloo &Fichier &Aide Accord de &licence &Quitter &Sauver dans un fichier &Scanner &Paramètres Une nouvelle version de Babiloo est disponible.

Nouvelle version : %s
Version actuelle : %s

Voulez-vous télécharger la nouvelle version dès maintenant ? A&uteurs Abréviation À &propos de Babiloo Avertissement Aspect Apparence Articles Compléter automatiquement Paramètres de Babiloo Arrière-plan Comportement Gras Parcourir: Construction de la fenêtre principale… Annuler Respecter la casse Vider Vider la zone de recherche Cliquer pour choisir une couleur Hauteur par défaut Largeur par défaut &Dictionnaires Nom du dictionnaire Dictionnaire non installable:
 %s Répertoire de stockage des dictionnaires: Ne pas disparaître Bas Télécharger Télécharger des dictionnaires Télécharger le dictionnaire Téléchargement en cours Téléchargement des fichiers… Élément Erreur Erreur pendant le chargement du dictionnaire.
Fichier corrompu ou non supporté. Erreur pendant le chargement du mot %s Exemple Explication Exporter le dictionnaire sous... Police de caractère Avant-plan À partir du lien : À partir de la liste : Paramètres globaux Aller à l'article &suivant Aller à l'article &précédent Historique Ignorer les accents Importer Informations Installer à partir d'un fichier... Dictionnaires installés Langue de l'interface : Italique L'adresse du lien est vide Chargement de %s... Gérer les dictionnaires... Déplacer vers le bas Déplacer vers le haut Nouvelle version détectée Ouvrir un Répertoire Veuillez commencer par charger un dictionnaire. Greffons Fenêtre popup Aperçu Im&primer l'article Prononcer le mot Prononcer les mots avec cette commande : Signaler un problème Reparcours des dictionnaires... Scan de la sélection Recherche Choisir un élément Choisir une police Choisir la taille de la police Informations succintes d'utilisation : Afficher si le mot est introuvable Afficher les informations Afficher des informations sur le dictionnaire Afficher uniquement sur appui d'une touche Taille Prononcer le &mot Début du téléchargement (%s bytes)... Le programme s'affichera dans la nouvelle langue après redémarrage. Le mot <b>%s</b> est introuvable. Délai avant disparition de la fenêtre de scan Titre Pour installer un nouveau dictionnaire à partir d'Internet, sélectionnez <b>Gérer les dictionnaires...</b> à partir du menu <b>Dictionnaires</b>, puis choisissez en un dans l'onglet <b>Télécharger des dictionnaires</b>. Pour installer un nouveau dictionnaire à partir d'un fichier, sélectionnez <b>Installer à partir d'un fichier...</b> depuis le menu <b>Dictionnaires</b>. Pour commencer à utiliser un dictionnaire, sélectionnez en un à partir du menu <b>Dictionnaires</b>. Nombre d'articles Transcription Traduire cette application… Traduire cette Application... Transparence Type Impossible de télécharger le fichier %s Souligné Haut Visiter la page d'accueil 