��    o      �  �         `	     a	     j	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	  �   �	     |
     �
     �
     �
  
   �
  	   �
     �
     �
     �
  
   �
     �
     �
     �
          "     (     =     S     b     p     }  &   �  
   �     �     �     �     �     �       E   	     O     o     w     �     �  
   �  
   �  
   �     �     �     �     �                    ;     @     U     l     �     �     �     �  	   �     �     �     �     	               &     5  #   H     l     }     �     �     �     �     �     �     �  	   
  !        6     T     Y  $   e  $   �  @   �      �  $        6  �   <  l   �  H   3     |     �     �     �     �     �  	   �     �     �  �     	   �     �  	   �  
   �     �     �     �                    -     4  	   H     R  �   b     �     �            
     
   &  
   1     <     J     d     j     y  "   �  "   �     �  �   �  !   i     �     �     �     �  '   �  
                  *     6     N     W  F   ]  '   �     �     �     �     �               *     :     T     o  	   �     �     �  "   �     �     �     �            &   %     L     [     w     }     �  (   �     �     �     �     �       '        B     X     u     �     �     �     �     �  &   �       )   %  /   O          �  $   �  )   �  <   �  +   '  8   S     �  �   �  ~   1  T   �               '     D     R  #   W  	   {     �  !   �     Q       :   N   
          H      b         $   G   ;   D   6   K                  @   g   ^   f       l   M   B       a      #   1   ?   h   *       L   k          `   O   "      &                        !       /   e       C          8       2           >       4   0                    P   5   W      (           X   [   Y   9       3   ]   m   S            +              R       U       _               F   J   %   o      E   )   =      7          c   ,                  \   	   j   I      A       .   V      <   n      T       '   Z   -       i   d            seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Building main dialog... Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary successfully installed:
 %s Don't hide Down Download Dictionaries Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Import one or many dictionaries Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The dictionary is already installed. The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-04-08 15:28+0100
PO-Revision-Date: 2009-04-08 14:05+0000
Last-Translator: Ivan Garcia <capiscuas@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-04-25 11:28+0000
X-Generator: Launchpad (build Unknown)
  segundos %s no fue cargado: %s C&ancelar &Acerca de &Acerca de Babiloo &Cerrar &Configurar Babiloo &Archivo &Ayuda &Acuerdo de licencia &Salir &Guardar al archivo &Explorar &Configuración Una nueva versión de Babiloo ha salido a la luz.

Versión nueva: %s
Versión actual: %s

Le gustaría descargar la nueva versión ahora? A&utores Abreviación Acerca de Babiloo Aviso Apariencia Apariencia Artículos AutoCompletar Configuración de Babiloo Fondo Comportamiento Negrita Construyendo diálogo principal... Distinguir mayusculas y minusculas Borrar This requirement definition document defines the requirements from product development area specified by General Insurance product management in Partenon. Hacer clic para seleccionar color Altura predeterminada Ancho predeterminado Diccionarios Nombre del diccionario Diccionario exitosamente instalado:
 %s No ocultar Abajo Descargar diccionarios Descargando Descargando ficheros... Elemento Error Error intentando cargar el diccionario.
Archivo corrupto o no válido. Error mientras se cargaba la palabra %s Ejemplo Explicación Exportar diccionario como... Tipo de letra Primer plano Desde un enlace: Desde la lista: Configuraciones Generales Ir al artículo &siguiente Ir al artículo &anterior Historial Ignorar acentos Importar Importar uno o varios diccionarios Info Instalar desde archivo... Diccionarios instalados Idioma de la interfaz: Cursiva La dirección del enlace está vacía. Cargando %s... Administrar diccionarios... Bajar Subir Nueva Versión Detectada Por favor primero cargue un diccionario. Extensiones Ventana emergente Vista previa &Imprimir artículo Pronunciar la palabra Pronunciar palabras usando este comando Comunicar un problema Reescaneando diccionarios... Escanear la selección Buscar Seleccionar elemento Seleccionar fuente Seleccione el tamaño de fuente Instrucciones de uso: Mostrar si la palabra no es encontrada Mostrar info Mostrar información sobre el diccionario Mostrar solo si el modificador está presionado Tamaño Pronunciar &palabra Empezando la descarga (%s bytes) ... El diccionario ya aparece como instalado. El nuevo idioma estará disponible al reiniciar el programa. La palabra <b>%s</b> no ha sido encontrada. Tiempo de espera antes de esconder el puntero del ratón Título Para instalar un diccionario nuevo desde el internet, seleccione <b>Organizar Diccionarios...</b> y escoja uno de la pestaña <b>Descargar Diccionarios</b>. Para instalar un diccionario nuevo desde un archivo, seleccione <b>Instalar de archivo...</b> en el menú <b>Diccionarios</b>. Para empezar usando un diccionario, seleccione uno del menú de <b>Diccionarios</b>. Artículos totales Transcripción Traducir Esta Aplicación... Transparencia Tipo No se puede descargar el fichero %s Subrayado Arriba Visitar la página web de Babiloo 