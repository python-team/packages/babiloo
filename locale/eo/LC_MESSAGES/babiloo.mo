��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   W     a     {     �     �     �     �     �     �  
   �     �     �     �     �  G   �  	   ?  
   I     T     `     g     o  	   w     �     �     �     �     �  
   �     �     �     �     �                .     =     N  
   W  )   b  !   �     �  	   �     �     �     �  
   �     �            D   !  !   f     �     �     �     �     �  
   �  	   �     �     �     �          !     7     ?     G     ]     q     �     �     �     �     �     �     �     �          1     ?     N     \     n  $   �     �     �     �     �     �     �          !     6     S     d     �     �     �  '   �  9   �        '   =     e  �   l  g   �  ?   Z     �     �     �     �  
   �     �                ,     3     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-11-03 09:03+0000
Last-Translator: Michael MORONI <haikara90@gmail.com>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekundoj %s ne estis ŝarĝata: %s Ĉesigi &Pri &Pri Babiloo &Fermi &Agordi Babiloo-n &Dosiero &Helpo &Permesilo &Forlasi &Konservi al dosiero &Skani &Agordoj Nova versio de Babiloo estas eldonita.

Nova versio: %s
Nuna versio: %s &Aŭtoroj Mallongigo Pri Babiloo Averto Aspekto Aspekto Artikoloj Aŭtomate kompletigi Agordoj de Babiloo Fono Konduto Grasa Foliumi... Kostruas ĉefdialogujon... Rezigni Usklecodistinga Viŝi Viŝi la serĉujon Alklaku por elekti koloron Defaŭlta alto Defaŭlta larĝo Vortaroj Vortarnomo Vortaro, kiu ne povis esti instalita:
 %s Dosierujo enhavanta la vortarojn: Ne kaŝi Malsupren Elŝuti Elŝuti vortarojn Elŝutanta vortaron Elŝutanta Elŝutanta dosierojn... Elemento Eraro Eraro dum malfermo de la vortaro.
Dosiero difektita aŭ nesubtenata. Eraro dum ŝargado de la vorto %s Ekzemplo Klarigo Eksporti vortaron kiel... Tiparo Malfono El ligilo: El listo: Mallokaj agordoj Iri al &sekva artikolo Iri al &antaŭa artikolo Historio Malatenti kromsignojn Importi Informo Instali el dosiero... Instalitaj vortaroj Interfaca lingvo: Kursiva Ligila adreso estas malplena Ŝargas %s... Administri vortarojn... Movi malsupren Movi supren Nova versio trovita Malfermi dosierujon Bonvolu unue malfermu vortaron. Kromprogramoj Ŝprucfenestro Antaŭrigardo &Printi artikolon Prononci la vorton Prononci vortojn uzante la komandon: Raporti problemon Reekzamenanta vortarojn... Skani elektaĵon Serĉi Elekti elementon Elekti tiparon Elekti tipargrandecon Koncizaj uzinformoj: Montri se vorto ne troviĝis Montri informojn Montri informojn pri vortaro Montru nur se modifilo premita Grando Elparoli la vorton Komencanta la elŝutadon (%s bajtoj)... La nova lingvo montriĝos post restartigo de la programo. La vorto <b>%s</b> ne troviĝis. Tempolimo antaŭ ol kaŝi post mustuŝo Titolo Por instali novan vortaron el interreto, elektu <b>Administri vortarojn...</b> kaj elektu unu el la <b>Elŝuti vortarojn</b>-langeto. Por instali novan vortaron el dosiero, elektu <b>instali el dosiero...</b> el la <b>Vortaroj</b>-menuo. Por starti uzi vortaron, elektu unu el la <b>Vortaro</b>-menuo. Nombro da artikoloj Transskripto Traduki la aplikaĵon Traduki ĉi tiun programaron... Travideblo Tipo Neeblas elŝuti la dosieron %s Substrekita Supren Viziti hejmpaĝon 