#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import Qt, SIGNAL

from core.languages import Languages, autodetect_lang, availablelanguages
from chooseLanguage_ui import Ui_ChooseLanguageDialog
import logging
log = logging.getLogger("babiloo.gui.chooseLanguage")

class chooseLanguageDialog(QtGui.QDialog):
    def __init__(self, parent, user_locale):
        QtGui.QDialog.__init__(self)
        self.ui = Ui_ChooseLanguageDialog()
        self.ui.setupUi(self)
        self._main  = parent
        self.connect(self.ui.languagesList, SIGNAL("activated(QModelIndex)"), self.onOkButton)
        self.connect(self.ui.OKButton, SIGNAL("clicked(bool)"), self.onOkButton)

        languages = {}
        for lang_locale in self._main.interface_langs:
                languageName = availablelanguages.locale2name(lang_locale)
                if not languageName:
                    languageName = lang_locale
                languages[languageName] = lang_locale
        langs = languages.keys()
        langs.sort()
        for lang in langs:
            item = QtGui.QListWidgetItem(lang)
            item.setData(Qt.UserRole, QtCore.QVariant(languages[lang]))
            self.ui.languagesList.addItem(item)
            try:
                if lang == user_locale:
                    self.ui.languagesList.setCurrentItem(item, QtGui.QItemSelectionModel.ClearAndSelect)
            except:
                print "Warning: Please upgrade to a PyQT version >= 4.4"


    def onOkButton(self):
        if not self.ui.languagesList.currentItem():
            QtGui.QMessageBox.about(self,"Alert","Please select a language")
        else:
            choosen_lang = str(self.ui.languagesList.currentItem().data(Qt.UserRole).toString().toUtf8())
            self._main.choosenLanguage = choosen_lang
            self.accept()

