#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt4.QtCore import Qt, SIGNAL
from PyQt4.Qt import QAbstractTableModel,  QVariant,  QModelIndex
from PyQt4.QtGui import QItemSelection


class HistoryListModel(QAbstractTableModel):
    def __init__(self, parent):
        QAbstractTableModel.__init__(self, parent)
        self._headers = ["Word"]
        self._main = parent
        self.rowSelected = None
        self.engine = parent.engine
        #check the list of dicts from registry
        self.wordlist = []
        self.wordDicts = {}
        self.definitions = {}
        self.dictionaryNames = {}

    def flags(self, index):
        flags = QAbstractTableModel.flags(self, index)
        return flags

    def getMainApp(self):
        return self._main

    def rowCount(self, parent=QModelIndex()):
        return len(self.wordlist)

    def columnCount(self, parent):
        return len(self._headers)

    def getWordFromIndex(self, index):
        row = index.row()
        if self.wordlist[row] != None:
                return self.wordlist[row]
        else:
                raise IndexError

    def headerData(self, section, orientation, role):
        return QVariant()

    def data(self, index, role):
        if index.isValid() and role == Qt.DisplayRole:
            row, col = index.row(), index.column()
            if self.wordlist[row] != None:
                    text = self.wordlist[row]
            else:
                    text = "Unknown"
            return QVariant(text)

        return QVariant()

    def getBackward(self):
        if self.canBackward():
                nextRow = self.rowSelected + 1
                word = self.wordlist[nextRow]
                rowSelection = QItemSelection(self.index(nextRow, 0),self.index(nextRow, 0))
                self.getMainApp().historyListSelectionModel.select(rowSelection, self.getMainApp().historyListSelectionModel.ClearAndSelect)
                return {'word':word,  'definition': self.getDefinition(word) ,  'dictionaryName': self.getDictionaryName(word)}

    def canBackward(self):
        self.updateSelection()
        return self.rowSelected < self.rowCount() - 1

    def getForward(self):
        if self.canForward():
                previousRow = self.rowSelected - 1
                word = self.wordlist[previousRow]
                rowSelection = QItemSelection(self.index(previousRow, 0),self.index(previousRow, 0))
                self.getMainApp().historyListSelectionModel.select(rowSelection, self.getMainApp().historyListSelectionModel.ClearAndSelect)
                return {'word':word,  'definition': self.getDefinition(word) ,  'dictionaryName': self.getDictionaryName(word)}

    def canForward(self):
        self.updateSelection()
        return self.rowSelected

    def clear(self):
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        self.wordlist = []
        self.definitions = {}
        self.wordDicts = {}
        self.emit(SIGNAL("layoutChanged()"))

    def getDefinition(self, word):
        return self.definitions[word]
    
    def findWord(self, word, dictId):
        return self.wordDicts.has_key(word) and self.wordDicts[word] == dictId
            
    def getDictionaryName(self, word):
        return self.dictionaryNames[word]

    def addWord(self, word, definition, dictName, dictId):
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        try:
                del self.wordlist[self.wordlist.index(word)]
        except:
                pass

        self.wordlist.insert(0, word)
        self.definitions[word]   = definition
        self.wordDicts[word] = dictId
        self.dictionaryNames[word] = dictName
        self.emit(SIGNAL("layoutChanged()"))

    def setWordList(self, words):
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        self.wordlist = words
        self.emit(SIGNAL("layoutChanged()"))

    def getWordList(self):
        return self.wordlist

    def updateSelection(self):
        selected = self.getMainApp().historyListSelectionModel.selection()
        if selected.count():
            self.rowSelected = selected.last().bottomRight().row()
        else:
            self.rowSelected = None
