#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt4.QtGui import QTextBrowser, QKeyEvent
from core.dictionary.dictionary import Definition
from PyQt4.QtCore import Qt
class DefinitionView(QTextBrowser):
        def __init__(self, parent):
                QTextBrowser.__init__(self, parent)
                self._frame = parent
                self._main = None

        def getMainApp(self):
            return self._main

        def keyPressEvent(self, event):
            if type(event) == QKeyEvent:
                #here accept the event and do something
                if event.modifiers() == Qt.NoModifier:
                        text = event.text()
                        if text:
                            self.getMainApp().articlesText.setFocus()
                            self.getMainApp().articlesText.keyPressEvent(event)
                            event.ignore()
                            return

            QTextBrowser.keyPressEvent(self, event)
        def setIntroduction(self):
            self.setHtml('<h3>%s</h3><ul><li>%s<br/></li>\n<li>%s<br/></li><li>%s</li></ul>' % (_("Short Usage Information:"),
                            _("To start using dictionary, select one from the <b>Dictionaries</b> menu."),
                            _("To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab."),
                            _("To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu.")))

        def setDefinition(self, word, definition = None,  dictionaryName = None):
            if dictionaryName == None:
                dictionaryName = self.getMainApp().engine.getDictionary(self.getMainApp().activeDictionary).getName()

            word_header = '<table BGCOLOR="#cccccc" WIDTH="100%%"><tr ><td ALIGN="left"><b>%s</b></td><td ALIGN="right">%s</td></tr></table><br>' % (word, dictionaryName)
            if definition == None:
                self.setHtml(word_header + _('The word <b>%s</b> is not found.') % word)
            else:
                if isinstance(definition, Definition):
                        self.setHtml(word_header + definition.content())
                else:
                        self.setHtml(word_header + definition)
